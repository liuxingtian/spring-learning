package com.xiaoming.module2;

import com.xiaoming.module2.service.AccountService;
import com.xiaoming.module2.utils.ConnectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

public class XmlTest {
    private ClassPathXmlApplicationContext context;

    @BeforeEach
    public void before() {
        context = new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    @Test
    public void testCreateBean() throws Exception {
        AccountService accountService = (AccountService) context.getBean("AccountService");
        accountService.update();
    }

    @Test
    public void testCreateStaticBean() {
        ConnectionUtils utils = (ConnectionUtils) context.getBean("staticConnectionUtils");
        System.out.println(utils);
    }

    @Test
    public void testMethodCreateBean() {
        ConnectionUtils utils = (ConnectionUtils) context.getBean("methodConnectionUtils");
        System.out.println(utils);
        context.close();
    }

}
