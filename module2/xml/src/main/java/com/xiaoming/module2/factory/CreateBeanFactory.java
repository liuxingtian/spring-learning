package com.xiaoming.module2.factory;

import com.xiaoming.module2.utils.ConnectionUtils;

public class CreateBeanFactory {

    public static ConnectionUtils getStaticConnectionUtils() {
        return new ConnectionUtils();
    }

    public ConnectionUtils getConnectionUtils() {
        return new ConnectionUtils();
    }

    public void init() {
        System.out.println("初始化前");
    }

    public void destory() {
        System.out.println("对象销毁");
    }
}
