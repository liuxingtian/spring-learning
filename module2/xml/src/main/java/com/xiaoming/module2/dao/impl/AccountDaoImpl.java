package com.xiaoming.module2.dao.impl;


import com.xiaoming.module2.dao.AccountDao;
import com.xiaoming.module2.utils.ConnectionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class AccountDaoImpl implements AccountDao {
    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    @Override
    public void updateAccount(String id, int count) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = connectionUtils.getCurrentThreadConn();
            // 执行SQL获取执行结果
            String sql = "update `account` set total = total + ? where id = ?";
            // 获取预处理statement
            preparedStatement = connection.prepareStatement(sql);
            // 设置参数
            preparedStatement.setInt(1, count);
            preparedStatement.setString(2, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
