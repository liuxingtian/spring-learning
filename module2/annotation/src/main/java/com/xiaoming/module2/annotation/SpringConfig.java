package com.xiaoming.module2.annotation;

import org.springframework.context.annotation.*;

import java.sql.Connection;
import java.sql.DriverManager;

@Configuration
@ComponentScan("com.xiaoming.module2.annotation")
@PropertySource({"classpath:jdbc.properties"})
public class SpringConfig {
    private String url;
    @Bean("connection")
    public Connection createConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://118.25.134.32/gifts?characterEncoding=utf-8&serverTimezone=GMT%2B8&useSSL=false";
        return DriverManager.getConnection(url, "root", "123456");
    }
}
