package com.xiaoming.module2.annotation.service.impl;


import com.xiaoming.module2.annotation.dao.AccountDao;
import com.xiaoming.module2.annotation.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;


public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void update() throws Exception {
        accountDao.updateAccount("1", 100);
        accountDao.updateAccount("2", -100);
        /*try {
            // 1.开启事务（关闭自动提交）
            TransactionManager.getInstance().beginTransaction();
            // 业务逻辑
            accountDao.update("1", 100);
            accountDao.update("2", -100);
            // 2.提交事务
            TransactionManager.getInstance().commit();
        } catch (Exception e) {
            e.printStackTrace();
            // 3.回滚事务
            TransactionManager.getInstance().rollback();
            throw e;
        }*/
    }

}
