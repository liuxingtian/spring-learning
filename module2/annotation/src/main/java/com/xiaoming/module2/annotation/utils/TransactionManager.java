package com.xiaoming.module2.annotation.utils;

/**
 * 事务开启、关闭
 */
public class TransactionManager {
    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    // 开启
    public void beginTransaction() throws Exception {
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }

    // 提交
    public void commit() throws Exception {
        connectionUtils.getCurrentThreadConn().commit();
    }

    // 回滚
    public void rollback() throws Exception {
        connectionUtils.getCurrentThreadConn().rollback();
    }
}
