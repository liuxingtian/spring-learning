package com;

import com.entity.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean-circle.xml");
        Student student = (Student) context.getBean("student");
        System.out.println(student);
    }
}
