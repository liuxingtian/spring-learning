package com.entity;

import lombok.Data;

@Data
public class Teacher {
    private String name;
    private int age;
    private Student student;

    public Teacher() {
        System.out.println("老师创建");
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
