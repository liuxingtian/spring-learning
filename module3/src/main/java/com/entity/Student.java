package com.entity;

import lombok.Data;

@Data
public class Student {
    private String name;
    private Integer age;
    private Teacher teacher;

    public Student(){
        System.out.println("学生创建");
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
