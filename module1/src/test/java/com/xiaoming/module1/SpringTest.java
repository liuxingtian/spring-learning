package com.xiaoming.module1;

import com.xiaoming.module1.factory.BeanFactory;
import com.xiaoming.module1.factory.ProxyFactory;
import com.xiaoming.module1.service.AccountService;
import com.xiaoming.module1.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SpringTest {
    private ProxyFactory proxyFactory;
    private AccountService accountService;

    @BeforeEach
    public void before() {
        proxyFactory = (ProxyFactory) BeanFactory.getBean("ProxyFactory");
        accountService = (AccountService) proxyFactory.getJdkProxy( BeanFactory.getBean("AccountService"));
    }

    @Test
    public void iocTest() throws Exception {

//        AccountServiceImpl accountServiceImpl = (AccountServiceImpl) BeanFactory.getBean("ProxyFactory");
//        accountService.update();
        // 从工厂获取委托对象
//        AccountService accountService = (AccountService) ProxyFactory.getInstance().getJdkProxy(accountServiceImpl);
        accountService.update();
    }
}
