package com.xiaoming.module1.service.impl;

import com.xiaoming.module1.dao.AccountDao;
import com.xiaoming.module1.service.AccountService;

public class AccountServiceImpl implements AccountService {
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void update() throws Exception {
        accountDao.updateAccount("1", 100);
        accountDao.updateAccount("2", -100);
        /*try {
            // 1.开启事务（关闭自动提交）
            TransactionManager.getInstance().beginTransaction();
            // 业务逻辑
            accountDao.update("1", 100);
            accountDao.update("2", -100);
            // 2.提交事务
            TransactionManager.getInstance().commit();
        } catch (Exception e) {
            e.printStackTrace();
            // 3.回滚事务
            TransactionManager.getInstance().rollback();
            throw e;
        }*/
    }

}
