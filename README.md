# Spring

Spring 是分层的全栈轻量级开源框架，以 IOC 和 AOP 为内核，提供了展示层 Spring MVC 和业务层事务管理等众多企业级应用技术，还能整合第三方框架和类库，是使用最多的 JAVA EE 企业级开源框架。

## IOC 和 AOP 

IOC、AOP 不是 Spring 提出的，只不过之前更加偏向理论化，在技术层面 Spring 做了非常好的实现。

### IOC 

**什么是 IOC ？**

IOC 全称 Inversion of Control（控制反转），这是一个技术思想，不是技术实践。
描述的是对象的创建、管理。

在传统开发中：一个类调用另一个类，往往需要在当前类中 new 一个对象；

IOC 思想下：不需要自己 new 对象，使用 IOC 容器，帮助实例化、管理对象，需要使用时通过 IOC 容器获取；

此时我们无需考虑如何创建对象，也不需要考虑对象的创建、管理。

控制：对象创建（实例化）的权利；
反转：控制器交给外部 Spring IOC 框架，IOC 维护对象依赖关系；

图2

**IOC 解决了什么问题？**

解决对象之间的耦合问题，

图3

**IOC 和 DI**

DI：Dependency Injection （依赖注入），IOC 和 DI 都描述了对象创建和关系维护，IOC 站在对象角度，对象实例化、管理的权利交给容器，DI 站在容器的角度，对象使用时，容器会把依赖的其它对象注入；


### AOP

**什么是 AOP？**

AOP：Aspect Oriented Programming（面向切面编程），是 OOP 的延续，OOP包含封装、继承、多态三大特性，OOP 是一种垂直继承体系，OOP 思想可以解决大多数代码重复性问题，但是有一些处理不了，在顶级父类中出现了代码重复（图6）；

在多个顺序流程中，出现的相同子流程代码称为横切逻辑代码，这些子流程称为点，多个流程称为面，横切逻辑的使用很有限，比如在事务、日志、权限校验。图7

横切逻辑的问题：横切代码逻辑重复、横切代码和主题业务无关，混杂在一起代码不好维护；


AOP 独辟蹊径，提出横向抽取机制，将横切逻辑代码和业务逻辑代码分离


**AOP 解决什么问题？**

在不改变原有代码逻辑的前提下，增强横向逻辑代码，从根本上解耦合。

**为什么叫面向切面编程？**

切：原有逻辑不能动，只能操作横切逻辑代码，所以面向横切逻辑；

面：横切逻辑代码往往要影响很多方法，每个方法如同一个点，多个点构成面；


### 手写 IOC、AOP



## Spring 使用

### Bean 实例化

  set方法、构造器注入

### 三种使用方式

纯XML

注解+XML：
  第三方的jar定义在xml，自己定义的写在注解中
  开启注解：
  <context:component-scan base-package="com.xiaoming"></context:component-scan>
  引入JDBC配置：
  <context:property-placeholder location="classpath:jdbc.properties"></context:property-placeholder>
  @Component、@Controller、@Service、@Repository
  @Autowired 按照类型注入，如果一个接口有多个实现，可以结合@Qualifier("id")

纯注解：
  @Configuration
  @ComponentScan  component-scan
  @PropertySource property-placeholder 引入配置文件
  @Value  读取配置文件属性
  @Import 关联多个类
  @Bean bean

### 高级使用

**bean 延迟加载（使用时创建）：**

Spring IOC 默认在服务启动时实例化所有 singleton bean

xml：lazy-init="true" 或者 beans 的default-lazy-init
@Lazy

不常用的对象可以延迟加载

可以查看 ApplicationContext的BeanFactory singletonObjects 数据

**FactoryBean 和 BeanFactory：**

BeanFactory是容器的顶级接口，定义了接口行为，负责生产、管理Bean；

Spring 的Bean 有两种：普通Bean、工厂Bean
我们可以借助FactoryBean自定义Bean的创建过程，类比Bean创建的静态方法、实例方法；

经常用于整合其他框架；

**后置处理器：**

BeanPostProcessor：bean对象实例化之后

BeanFactoryPostProcessor：bean实例化前，BeanFactory实例化后

SpringBean生命周期：
读取xml、反射实例化、设置属性值、调用BeanNameAware的setBeanName、调用BeanFactoryAware的setBeanFactory方法、调用ApplicationContextAware的setApplicationContext、
调用BeanPostProcessor的预初始化方法（两个方法postProcessBeforeInitialization、postProcessAfterInitialization）、调用InitialzingBean的afterPropertiesSet方法
、调用init-method方法、

如果是prototype立即交付，如果是singleton放入缓存池 销毁时调用DisposableBean的destory和destory-method方法

读取xml涉及到BeanDefinition封装xml bean标签信息

英语Aware：发现

## Spring IOC 源码

原则：抓主线、关注源码解构和业务流程，淡化某一行编写细节

技巧：断点（调用栈）、反调（Find Usage）

编译：core、oxm、 context、beans、aspects、aop等等
    工程-》tasks-》compilerTestJava

**BeanFactory创建流程：**

ApplicationContext 是容器的高级接口，BeanFactory是顶级容器，官方称为IOC容器（map IOC容器的单例池，容器是一组组件和过程的集合，不仅仅是单例池），

ApplicationContext实现：
  AutowireCapableBeanFactory：
  HierarchicalBeanFactory：父容器
  ListableBeanFactory：新增批量操作接口
  MessageSource：国际化
  ResourceLoader：加载资源（xml、properties等等）

IOC容器创建管理Bean，Bean有生命周期，
无参构造器、初始化方法、Bean后置处理器的before/after：AbstractApplicationContext 的 finishBeanFactoryInitialization
Bean工厂后置处理器：AbstractApplicationContext 的 invokeBeanFactoryPostProcessors
Bean的后置处理器：AbstractApplicationContext 的 registerBeanPostProcessors

**BeanDefinition加载、解析注册流程：**

**Bean创建流程：**

**延迟加载流程：**

**Spring IOC 循环依赖问题处理：**
